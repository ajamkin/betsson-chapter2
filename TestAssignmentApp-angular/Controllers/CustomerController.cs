﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.OData;
using System.Web.OData.Query;

namespace TestAssignmentApp.Controllers
{
    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
    }

    public class CustomersController : ApiController
    {
        public List<Customer> Customers
        {
            get
            {
                if (HttpContext.Current.Cache["customers"] == null)
                    HttpContext.Current.Cache["customers"] = prepareCustomers();
                return (List<Customer>)HttpContext.Current.Cache["customers"];
            }
        }

// ReSharper disable once ReturnTypeCanBeEnumerable.Global
        [EnableQuery(AllowedQueryOptions = AllowedQueryOptions.All)]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IQueryable<Customer> Get()
        {
            return Customers.AsQueryable();
        }


        private List<Customer> prepareCustomers()
        {
            var result = new List<Customer>();
            for (int counter = 0; counter <= 100; counter++)
            {
                var c = new Customer()
                {
                    Address = Faker.Address.StreetAddress(),
                    FirstName = Faker.Name.First(),
                    LastName = Faker.Name.Last(),
                    Company = Faker.Company.Name(),
                    City = Faker.Address.City(),
                    State = Faker.Address.State(),
                    ZipCode = Faker.Address.ZipCode(),
                    PhoneNumber = Faker.Phone.Number(),
                    EmailAddress = Faker.Internet.Email()
                };
                result.Add(c);
            }
            return result;
        } 

    }
}
