﻿var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require('path');

module.exports = {
    entry: './app/entry.js',
    output: {
        path: './scripts',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { test: /app.*\.js$/, loaders: ['ng-annotate'] },
            { test: /\.css/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader') },
            { test: /\.(otf|eot|svg|ttf|woff)/, loader: 'url' }
        ]
    },
    plugins: [
        new ExtractTextPlugin("styles.css")
    ]
};