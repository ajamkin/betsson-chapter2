﻿require('ODataResources/build/odataresources.js');

var customersModule = angular.module('sampleApp.customers', ['ODataResources']);

customersModule
    .service('CustomerService', require('./services/customers.service.js'))
    .controller('CustomerListController', require('./controllers/customer-list.controller'))
    .directive('sortableHeaderFor', require('./directives/sort-header.directive'))
    .filter('customerLocationDisplay', require('./filters/customer-location-display.filter.js'));

module.exports = customersModule;