﻿module.exports = SortHeaderDirective;

function SortHeaderDirective() {
	return {
		transclude: true,
		require: 'ngModel',
		scope: {
			field: '@sortableHeaderFor'
        },
		link: function (scope, elem, attr, ngModelCtrl) {
			scope.toggleOrder = function () {
				scope.direction = (scope.direction == 'desc' ? 'asc' : 'desc');
				ngModelCtrl.$setViewValue({
					field: scope.field,
					direction: scope.direction
				});
			}

		    scope.$watch(function() {
		        return ngModelCtrl.$modelValue;
		    }, function(sortData) {
		    	if (sortData) {
		    		scope.showArrows = sortData.field == scope.field;
		    	}
		    });
		},
        template:
			'<a style="cursor:pointer;white-space: nowrap;" ng-click="toggleOrder()">' +
				'<ng-transclude></ng-transclude>' +
				'<span class="order" ng-show="showArrows" ng-class="{dropup: direction == \'desc\'}">' +
					'<span class="caret" style="margin: 10px 5px;"></span>' +
				'</span>' +
			'</a>'
    }
}