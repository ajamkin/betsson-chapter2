﻿module.exports = CustomersService;

/*@ngInject*/
function CustomersService($odataresource) {
    return $odataresource('/api/customers/:customerId', { customerId: '@id' });
}