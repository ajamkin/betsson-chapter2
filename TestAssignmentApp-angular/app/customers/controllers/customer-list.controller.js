﻿module.exports = CustomerListController;

/*@ngInject*/
function CustomerListController(CustomerService, $odata, $) {
    var vm = this;

    vm.customers = [];
    vm.pageChanged = pageChanged;
    vm.itemsPerPage = 10;
    vm.totalItems = 100;
    vm.sortData = {
        field: 'FirstName'
    };

    pageChanged();

    function pageChanged() {
        var _query = CustomerService.odata();

        if (vm.search) {
            appendFilter(_query);
        }

        vm.customers = _query.skip(vm.itemsPerPage * (vm.currentPage - 1))
            .take(vm.itemsPerPage)
            .orderBy(vm.sortData.field, vm.sortData.direction || 'asc')
            .query();
    }

    function appendFilter(query) {
        return query.filter($odata.Predicate.or($.map(['FirstName', 'LastName', 'Company', 'PhoneNumber', 'EmailAddress', 'Address', 'City', 'State', 'ZipCode'], function(fieldName) {
            return new $odata.Predicate(new $odata.Func("contains", fieldName, vm.search));
        })));
    }

}
