﻿module.exports = customerLocationDisplay;

function customerLocationDisplay() {
    return function(customer) {
        return customer.Address + ', ' + customer.Zip + ' ' + customer.City + ', ' + customer.State;
    }
}