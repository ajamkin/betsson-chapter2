﻿var angular = require('angular');
var uibs = require('angular-ui-bootstrap');

var app = angular.module('sampleApp', [
    require('./customers/customers').name,
    'ui.bootstrap'
]);

app.constant('$', require('jquery'));

module.exports = app;