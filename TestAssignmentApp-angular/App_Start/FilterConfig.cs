﻿using System.Web;
using System.Web.Mvc;

namespace TestAssignmentApp_angular
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
